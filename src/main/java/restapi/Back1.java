package restapi;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.javalin.http.Context;

public class Back1 {
  final static Logger logger = LoggerFactory.getLogger(Back1.class);
  private static int cnt = 0;
  public static void helloHandler(final Context ctx) {
    cnt++;
    Utils.respond(ctx, "Hello World from Back1 " + cnt, 0);
  }

  public static void helloHandlerDelay(final Context ctx) {
    cnt++;
    Utils.respond(ctx, "Hello Delay from Back1 " + cnt, Utils.randomDelay(2000, 8000, 10));
  }

  public static void helloHandlerError(final Context ctx) {
    cnt++;
    try {
      thrower();
    } catch (Exception e) {
      logger.error("Error thrown " + e.getMessage() , e);
      Utils.respondErr(ctx, "Hello Error from Back1 " + e.getMessage(), 0);
      return;
    } 
  }

  public static void helloHandlerErrorDelay(final Context ctx) {
    cnt++;
    try {
      thrower();
    } catch (Exception e) {
      logger.error("Error thrown " + e.getMessage() , e);
      Utils.respondErr(ctx, "Hello Delay & Error from Back1 " + e.getMessage(), Utils.randomDelay(2000, 8000, 10));
      return;
    } 
  }

  public static void thrower() throws Exception {
    String msg = "Throwing error @ " + new Date();
    Exception e = new Exception(msg);

    throw e;
  }
}