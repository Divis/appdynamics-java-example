package restapi;

import io.javalin.http.Context;
import java.math.*;

public class Utils {

  public static void respond(final Context ctx, final String response, final long delay) {

    final Object lock = new Object();

    final java.util.Timer timer = new java.util.Timer();
    timer.schedule(new java.util.TimerTask() {
      @Override
      public void run() {
        System.out.println("Sending response: " + response);
        ctx.result(response);
        synchronized (lock) {
          lock.notifyAll();
        }
      }
    }, delay);

    synchronized (lock) {
      try {
        lock.wait();
      } catch (final InterruptedException ex) {
      }
    }
  }

  public static void respondErr(final Context ctx, final String response, final long delay) {

    final Object lock = new Object();

    final java.util.Timer timer = new java.util.Timer();
    timer.schedule(new java.util.TimerTask() {
      @Override
      public void run() {
        System.out.println("Sending response: " + response);
        ctx.status(500);
        ctx.result(response);
        synchronized (lock) {
          lock.notifyAll();
        }
      }
    }, delay);

    synchronized (lock) {
      try {
        lock.wait();
      } catch (final InterruptedException ex) {
      }
    }
  }

  public static long randomDelay(final long min, final long max, final long probability) {
    long delay = 0;
    final long random100 = (long) (Math.random() * 100);
    if (random100 < probability) {
      delay = (long) (min + (max - min) * Math.random());
    }
    return delay;
  }

  public static String getenv(String variable, String defaultValue) {
    String envVar = System.getenv(variable);
    return envVar == null ? defaultValue : envVar;
  }

  public static boolean getenvBoolean(String variable, boolean defaultValue) {
    String envVar = System.getenv(variable);
    System.out.println("GetEnv: " + variable + " -> " + envVar);
    return envVar == null ? defaultValue : envVar.equalsIgnoreCase("true");
  }

  public static int getenvInt(String variable, int defaultValue) {
    String envVar = System.getenv(variable);
    System.out.println("GetEnv: " + variable + " -> " + envVar);
    
    int val;
    try {
       val = Integer.parseInt(envVar);
    } catch (Exception e) {
      val = defaultValue; 
    }

    return val;
  }
}