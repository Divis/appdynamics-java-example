package restapi;

abstract class ServiceWrapper {
  public abstract void connect();

  public abstract void connect(String host, String port);

  public abstract boolean isConnected();

  public abstract void close();

  public abstract void initDB();

  public void ensureConnected(String host, String port, boolean init) {
    new Thread(new Runnable() {

      @Override
      public void run() {
        boolean initted = false;
        while (true) {
          System.out.println("Connected to " + host + ":" + port + " -> " + isConnected());
          if (!isConnected()) {
            connect(host, port);
          }
          if (isConnected() && !initted && init) {
            initDB();
            initted = true;
          }
          try {
            Thread.sleep(5000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
		  }
    }, host + "-" + port).start();;
  }
}