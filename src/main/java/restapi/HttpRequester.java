package restapi;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpRequester {
  final static Logger logger = LoggerFactory.getLogger(HttpRequester.class);

  public String httpGetRequest(String url) {
    CloseableHttpClient httpclient = HttpClients.createDefault();
    HttpGet httpGet = new HttpGet(url);
    CloseableHttpResponse response = null;
    HttpEntity entity = null;
    StatusLine status = null;
    try {
      response = httpclient.execute(httpGet);

      System.out.println(response.getStatusLine());
      entity = response.getEntity();
      status = response.getStatusLine();
      // do something useful with the response body
      // and ensure it is fully consumed
    } catch (Exception e) {
      logger.error("Error in http access to url " + url + " : " + e.getMessage() , e);
      e.printStackTrace();
    } finally {
      try {
        response.close();
        return status + ":" + entity != null ? EntityUtils.toString(entity) : null;
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return "";
  }
}