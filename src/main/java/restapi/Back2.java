package restapi;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.javalin.http.Context;

public class Back2 {
  final static Logger logger = LoggerFactory.getLogger(Back2.class);
  public static int cnt = 0;

  public static void helloHandler(final Context ctx) {
    cnt++;
    Utils.respond(ctx, "Hello World from Back2 " + cnt, 0);
  }

  public static void helloHandlerDelay(final Context ctx) {
    cnt++;
    Utils.respond(ctx, "Hello World from Back2 " + cnt, Utils.randomDelay(2000, 8000, 10));
  }

  public static void helloHandlerError(final Context ctx) {
    cnt++;
    try {
      thrower();
    } catch (Exception e) {
      logger.error("Error thrown " + e.getMessage() , e);
      Utils.respond(ctx, "Hello Error from Back2 " + e.getMessage(), 0);
      return;
    } 
  }

  public static void helloHandlerErrorDelay(final Context ctx) {
    cnt++;
    try {
      thrower();
    } catch (Exception e) {
      logger.error("Error thrown " + e.getMessage() , e);
      Utils.respond(ctx, "Hello Delay & Error from Back2 " + e.getMessage(), Utils.randomDelay(2000, 8000, 10));
      return;
    } /*finally {
      Utils.respond(ctx, "Hello Delay & Error from Back2 " + cnt, Utils.randomDelay(2000, 8000, 10));
    }*/
  }

  public static void thrower() throws Exception {
    int i = 1;
    int j = 0;
    int k = i / j;
    System.out.println(k);
  }
}