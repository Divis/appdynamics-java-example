package restapi;

import io.javalin.http.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrxSplit {
  final static Logger logger = LoggerFactory.getLogger(TrxSplit.class);

  private static int cnt = 0;

  public static void cookieSplitHandler(final Context ctx) {
    cnt++;
    int cookieVal = (int) (Math.random() * 8);
    String response = "Cookie split handler from TrxSplit " + cnt + " cookie value " + cookieVal;
    ctx.cookie("myCookie", "id" + cookieVal);
    ctx.cookie("myOtherCookie", "id" + (cookieVal * 2));
    ctx.result(response);
  }

  public static class ReturnData {
    public int status;
    public String data;

    public int getStatus() {
      return status;
    }
  }

  private static Integer[] codes = { 200, 400, 401, 403, 500 };

  public static void httpCodeSplitHandler(final Context ctx) {
    int statusCode = httpCodeSplitSubHandler();
    String result = "Status code " + statusCode;
    ctx.status(statusCode);
    ctx.result(result);
  }

  public static int httpCodeSplitSubHandler() {
    int statusCode = codes[(int) (Math.random() * codes.length)];

    return (statusCode);
  }

  public static int cntHdr = 0;

  public static void customSplitHandler(final Context ctx) {
    cntHdr++;
    String response = "Custom split handler from TrxSplit " + cntHdr;
    ctx.header("myHeader", "id" + cntHdr + ";value" + (cntHdr * cntHdr));
    ctx.header("myOtherHeader", "id" + (cntHdr * 2));
    ctx.result(response);
  }

  public static int cntThr = 0;

  public static void customThrowHandler(final Context ctx) throws Exception {
    cntThr++;
    String response = "";

    try {
      thrower();
    } catch (Exception e) {
      response += "Error Msg: " + e.getMessage() + " ";
      logger.error("Error thrown " + e.getMessage() , e);
      e.printStackTrace();
    } finally {

      response += "Custom split handler from TrxSplit " + cntThr;
      ctx.header("myHeader", "id" + cntThr + ";value" + (cntThr * cntThr));
      ctx.header("myOtherHeader", "id" + (cntThr * 2));
      ctx.result(response);

    }
  }

  public static String msgs[] = {"I have no clue", "A je to v haji", "To by se nikdy nemelo stat"};
  public static void thrower() throws Exception {
    String msg = msgs[(int)(Math.random() * msgs.length)];
    Exception e = new Exception(msg);

    throw e;
  }
}