# AppDynamics Java Example

# How To

Prerequisites:
Java 11 installed, for example Amazon Corretto: https://docs.aws.amazon.com/corretto/latest/corretto-11-ug/macos-install.html

Gradle build tool installed (verified with version 7.3.3), for example on Mac brew install gradle

1) clone this repo: git clone https://gitlab.com/Divis/appdynamics-java-example.git

2) cd to AppServerAgent-20.8.0.30686/ver20.8.0.30686/conf directory

3) copy controller-info.xml-template to controller-info.xml in the same directory

4) edit controller-info.xml with correct controller and application information - the tags of interest are:
   <controller-host>
   <controller-port>
   <controller-ssl-enabled>
   <application-name>
   <tier-name>
   <node-name>
   <account-name>
   <account-access-key>

5) build the app: gradle build

   application is built into directory build/libs as a fat jar

   cd to build/libs

6) run the app with AppDynamics Agent:

   java -javaagent:../../AppServerAgent-20.8.0.30686/javaagent.jar -jar restapi.jar

in AppDynamics controller, you should get the application specified in the controller-info.xml file

7) Run some requests against the application - open url http://localhost:4567/man.html in your browser

At this stage, you can try urls:

/hello

/api/self/hello

/api/nonexistent

run a few requests, wait a few minutes and check in AppD controller what you see for your application


# Running the example as a multi-tier application


