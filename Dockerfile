FROM amazoncorretto:11-alpine3.15-jdk

RUN mkdir /app
COPY build/libs /app
WORKDIR /app

EXPOSE 4567

CMD ["java","-jar","restapi.jar"]

